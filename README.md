# How to start

[Slidev](https://github.com/slidevjs/slidev) can be used to generate, play and export slides.

## Start the project with default file

To start executing the project first make sure you have node installed (I recommend using [nvm](https://github.com/nvm-sh/nvm) and installing the latest stable version). Once you have it you can execute the following commands.

Then it's required pnpm, which can be installed with:

```bash
$ npm install -g pnpm
```

Then in the `urjc-infind-slides` folder execute:

```bash
$ pnpm install
```

## Build entired project

To generate all the slides and the website:

```bash
$ pnpm run build
```

## Edit existing slides

Go to the folder, for example `slides > 2022-01-31 > src` and execute:

```bash
$ pnpm run dev
```

## Export pdf from slides

Go to the folder, for example `slides > 2022-01-31 > src` and execute:

```bash
$ pnpm run export
```

## Generate new set of slides

To generate new slides execute:

```bash
$ cd slides
$ cp -r template 2022-02-07
```

Then edit `slides/2022-02-07/src/package.json` and change all reference to `yyyy`, `mm` and `dd`to meet current date.

```json
{
    "version": "0.0.0",
    "private": true,
    "scripts": {
      "dev": "slidev",
      "build": "slidev build --base /yyyy/mm/dd --out ../../../dist/yyyy/mm/dd",
      "export": "slidev export --dark --output ../yyyy-mm-dd.pdf"
    }
}
```

would be:

```json
{
    "version": "0.0.0",
    "private": true,
    "scripts": {
      "dev": "slidev",
      "build": "slidev build --base /2022/02/07 --out ../../../dist/2022/02/07",
      "export": "slidev export --dark --output ../2022-02-07.pdf"
    }
}
```

Then you can edit file `slides/2022-02-07/src/slides.md` and execute:

```bash
$ pnpm dev
```

## Update index of slides

By now, list of slides is done by hand so we have to edit file `/urjc-infind-slides/list-slides-web/src/pages/index.astro`

There we have to find slides list and add the new line

```javascript
...

let slides = [
	{url: '/2022/01/31/', name: 'Slides'},
	{url: '/2022/02/07/', name: 'Slides'}, // new element
];

...
```