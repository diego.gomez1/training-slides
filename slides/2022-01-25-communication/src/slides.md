---
title: Comunicación en organizaciones
theme: ../../../theme-divriots/
info: |
  ## Escuela IDEO
  Servicio de Tecnología.

  Autor:  
  - Diego Gómez Díaz <diego.gomez@escuelaideo.edu.es>
permalink: https://slides.techer.es/2022-01-25-communication/
twitter: diegogd
author: diegogd
dates:
  - name: Formación TIC
    logo: ideo_white.svg
    datetime: 2022-01-25T15:15:00.000Z
colorSchema: dark
---

<p class="tagline">Competencias digitales</p>

# Comunicación en organizaciones

<!--
Introducción sobre la necesidad de reflexionar sobre cualquier proceso de la escuela. Dedicar tiempo a pensar estos procesos puede repercutir en la eficacia y eficiencia del equipo.
-->

---

# Contenido

1. ¿Qué comunico? ¿Cómo lo comunico?
2. Herramientas existentes en el mercado
  - <logos-discord-icon class="mr-2" /> Ejemplo con Discord 
3. Opciones en el ecosistema de Google
  - <logos-google-calendar class="mr-2" /> Buenas prácticas de Calendar
  - <logos-google-gmail class="mr-2" /> Buenas prácticas de Gmail 
  - <logos-google-drive class="mr-2" /> Buenas prácticas en Drive
  - <logos-google-icon class="mr-2" /> Entender workspaces
4. Acciones para el cambio

---
layout: media
url: <Gif id="j0SrVwV9e95qWm7Gs2" caption="Web is 30 years old" />
variant: left
overlay: true
---

# Primera parte

## ¿Qué comunico? 

## ¿Cómo lo comunico?

---
layout: media
url: ./img/ideo-img2.jpg
variant: right
ratio: 2/3
links:
  - bit.ly/32oQhxq
---

<Figure src="./img/ideo_white.svg" class="h-32 mb-10" />

# ¿Cómo nos comunicamos en IDEO?

<qrcode link="https://bit.ly/32oQhxq" class="flex justify-center" />

<!--
Comunicar el cambio
-->

---
layout: iframe
url: https://app.mural.co/embed/03cc54fd-94df-40f4-8e24-8e8bdac3f999
links:
 - bit.ly/32oQhxq
---

---
layout: media
url: <Gif id="xUOwGhauv1d6nceRbi" caption="Funciona el correo?" />
variant: right
ratio: 2/3
overlay: true
links: 
  - https://bit.ly/3nShVud
---

# ¿Funciona el **Correo**?

Plantea situaciones en las qué sí en las que no.

<qrcode link="https://bit.ly/3nShVud" class="flex justify-end" />

---
layout: iframe
url: https://bit.ly/3nShVud
---

---

## Antes

- Email como paradigma familiar
- Asíncrono
- Web informativa
- Hilos de mensajes

::col2::

## Ahora

- Web colaborativa
- Comunicación oblicua (te persigue)
  - Multidispositivo
- IA 
  - <mdi-arrow-up-bold-outline /> Coste tiempo persona 
  - <mdi-arrow-down-bold-outline /> tiempo máquina

::header::

<div class="text-center mb-10">

# Cambio de paradigma

Aparición de web 2.0 (hace 21 años) <noto-v1-confused-face />

Información unidiccional vs colaboración

</div>

---
layout: media
url: <Gif id="G5n8sqIOxBqow" caption="Probemos Discord" />
variant: left
ratio: 1/2
overlay: true
---

# Problemas frecuentes

- Coste de adopción
- Amortización de aprendizajes
- Sentirse abrumado por la herramienta
- Adopción de herramientas equivocadas  
- Confundir los objetivos de la comunicación

---
layout: media
url: <Gif id="Nvdt7Imy0TOJkDTRv6" caption="Probemos Discord" />
variant: left
ratio: 1/2
overlay: true
---

# Optimismo para el cambio

- Herramientas diseñadas en torno al usuario
- Compensa el tiempo invertido

---

# Claves del diseño de la comunicación

```mermaid {theme: 'neutral', scale: 1.0}
flowchart LR
  id1[[Entender la\n comunicación]] --> id2(Centrar el foco\ndel mensaje) --> id3(Identificar las \nherramientas adecuadas) --> id4(Llegar a un consenso\nde uso)
  id1 --> idu(Entender usuarios\ndinámicas y necesidades)
  idu --> id3    
```

---

## Objeto de comunicación

- Evento próximo o periódico
- Información efímera
- Proyecto
- Documento

::col2::

## Equipo

- Buscar la **eficacia** del equipo
- Extender **ecosistema existente**
- **Herramientas comunes** frente a preferencias individuales

::header::

# Diseño de la comunicación

---
layout: section
background: https://images.unsplash.com/photo-1616531770192-6eaea74c2456
---

# ¿Qué soluciones existen?

Ejemplo sesgado, podríamos analizar más casos

---

# Orientado al **evento** o espacio

<div class="grid grid-cols grid-cols-2 mt-10 items-center justify-items-center">

<logos-google-calendar class="text-8xl" />
<logos-microsoft-teams class="text-8xl" />

</div>

---

# Comunicación **efímera**

<div class="grid grid-cols grid-cols-3 mt-10 items-center justify-items-center">

<logos-telegram class="text-8xl" />
<logos-whatsapp class="text-8xl" />
<whh-hangouts class="text-8xl text-green-700" />

</div>

---

# Comunicación en torno a un **Proyecto**

<div class="grid grid-cols grid-cols-3 mt-10 items-center justify-items-center gap-5">

<logos-trello class="text-7xl" />
<logos-gitlab class="text-7xl" />
<logos-github class="text-9xl -m-y-16" />
<div></div>
<img src="/img/codecks-logo.svg" class="w-[200px]" />

</div>

---

# Comunicación en torno a un **Objeto**

<div class="grid grid-cols grid-cols-2 mt-10 items-center justify-items-center gap-5">

<logos-google-drive class="text-7xl" />
<img src="/img/odoo_logo.svg" class="w-[200px]" />

</div>

---

# Comunicación en torno a **focos de interés**

<div class="grid grid-cols grid-cols-3 mt-10 items-center justify-items-center gap-1">

<logos-slack class="icon" />
<logos-discord class="icon" />
<logos-mattermost class="icon" />
<div></div>
<logos-rocket-chat class="icon" />

</div>

<style>
  .icon {
    @apply text-12xl -m-y-16;

  }
</style>

---
layout: section
background: https://images.unsplash.com/photo-1627634777217-c864268db30c
---

# Comunicación de comunidades

---
layout: media
url: <Gif id="3NtY188QaxDdC" caption="Probemos Discord" />
variant: right
ratio: 2/3
overlay: true
links: 
  - https://discord.gg/38dft2Y2
---

# Probemos <logos-discord-icon /> **Discord**

Probemos el entorno de discord.

<qrcode link="https://discord.gg/38dft2Y2" class="flex justify-end" />

---
layout: section
background: ./img/ideo-img3.jpg
---

# ¿Qué tenemos en IDEO?


---
layout: media
url: <Gif id="kEKcOWl8RMLde" caption="Probemos Discord" />
variant: left
ratio: 1/2
overlay: false
---

# A cambiarlo todo

Una herramienta nueva cada día

---

Ecosistema de Google
  - <logos-google-calendar class="mr-2" /> Buenas prácticas de Calendar
  - <logos-google-gmail class="mr-2" /> Buenas prácticas de Gmail 
  - <logos-google-drive class="mr-2" /> Buenas prácticas en Drive
  - <logos-google-icon class="mr-2" /> Entender workspaces

::col2::

¿Por qué?
  - Integración
  - Resistencia al cambio
  - Buenas herramientas
  - **Falta usarlo**

::header::

# Usar nuestros recursos

---
layout: center
---

# <logos-google-calendar class="mr-2" /> Calendar

- Búsqueda de personas
- Búsqueda de espacios
- Organización de reuniones
- Comunicación de reuniones

---
layout: center
---

# <logos-google-gmail class="mr-2" /> Gmail

- Se asume mal uso
- Comprender cómo llegar a personas concretas
  - **To**, **CC** y **grupos**
- Modo Conversación
- Uso de filtros
- Acciones asociadas
- Acciones inteligentes

---
layout: center
---

# <logos-google-drive class="mr-2" /> Drive

- Ventaja de usar Google Drive vs Office
- Cambios desde última apertura
- Accesos
- Comentarios en el documento
- Comentarios dirigidos

---
layout: center
---

# <logos-google-icon class="mr-2" /> Workspaces

- Esfuerzo por centrar el trabajo del equipo
- Herramientas para la gestión personal
- Comunicación ágil y complementaria
  - Chat
  - Tareas
  - Comentarios en documentos
- Diferencias entre hilos de conversación
- Nuevas reglas en la comunicación
  - Reducir ruido
  - No más gracias!!! <twemoji-thumbs-up />

---
layout: section
background: https://images.unsplash.com/photo-1618556391870-aa21debfe537
---

# Acciones para el cambio

---

# Propuesta

Al final de trimetres pasaremos un formulario para ver si hemos incorporado algún cambio en nuestra visión.

- ¿Comunicación **efectiva**?
- ¿Trabajamos como **equipo**?
- ¿Pensamos en la **mejora**?
- ¿Tenemos la **herramienta** adecuada?

---
layout: qa
---

---
layout: thanks
---
