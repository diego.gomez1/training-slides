---
theme: ../../../theme-divriots/
highlighter: shiki
lineNumbers: true
info: |
  ## Escuela IDEO
  Servicio de Tecnología.

  Autor:  
  - Diego Gómez Díaz <diego.gomez@escuelaideo.edu.es>
# persist drawings in exports and build
drawings:
  persist: false
layout: cover
permalink: https://slides.techer.es/organization/
twitter: diegogd
author: diegogd
---

# Presentatation

---

<Giphy id="FspLvJQlQACXu" caption="Here's a Story of Interfaces" class="h-4/5" />

---
layout: media
---


---

# Otra diapositiva

Es importante